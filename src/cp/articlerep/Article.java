package cp.articlerep;

import cp.articlerep.ds.LinkedList;
import cp.articlerep.ds.List;
import cp.articlerep.ds.Iterator;

/**
 * @author Ricardo Dias (2013)
 * @author João Lourenço (2020)
 */
public class Article {
	
	private int id;
	private String name;
	private final List<String> authors;
	private final List<String> keywords;

	public Article(final int id, final String name) {
		this.id = id;
		this.name = name;
		this.authors = new LinkedList<String>();
		this.keywords = new LinkedList<String>();
	}

	public void addAuthor(final String author) {
		this.authors.add(author);
	}

	public void addKeyword(final String keyword) {
		this.keywords.add(keyword);
	}

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public String toString() {
		String r = "\n(" + id + ", " + name;
		int i;

		r += ",[";
		i = 0;

		for (final Iterator<String> it = authors.iterator(); it.hasNext();) {
			r += (i > 0 ? "," : "") + it.next();
			i++;
		}

		r += "],[";
		i = 0;

		for (final Iterator<String> it = keywords.iterator(); it.hasNext();) {
			r += (i > 0 ? "," : "") + it.next();
			i++;
		}

		r += "])";

		return r;
	}
}
