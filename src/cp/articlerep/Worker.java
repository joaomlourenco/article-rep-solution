package cp.articlerep;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import cp.articlerep.ds.Iterator;
import cp.articlerep.ds.LinkedList;
import cp.articlerep.ds.List;

/**
 * @author Ricardo Dias (2013)
 * @author João Lourenço (2020)
 */
public class Worker {

	public static final boolean DO_VALIDATION = Boolean.parseBoolean(System
			.getProperty("cp.articlerep.validate"));

	private int dictSize;
	private final String dictFile;
	private final int put;
	private final int del;
	private final int get;
	private final int authors;
	private final int keywords;
	private final int findList;

	private String[] wordArray;
	private Thread[] workers;
	private Job[] jobs;

	private final Repository repository;

	private volatile boolean running;
	private volatile boolean pause;

	private int totalOperations;

	/**
	 * @param dictSize
	 * @param dictFile
	 * @param put
	 * @param del
	 * @param get
	 * @param authors
	 * @param keywords
	 * @param findList
	 */
	public Worker(final int dictSize, final String dictFile, final int put, final int del, final int get,
			final int authors, final int keywords, final int findList) {
		this.dictSize = dictSize;
		this.dictFile = dictFile;
		this.put = put;
		this.del = del;
		this.get = get;
		this.authors = authors;
		this.keywords = keywords;
		this.findList = findList;

		populateWordArray();

		this.workers = null;
		this.jobs = null;

		this.repository = new Repository(dictSize);

		this.running = true;
		this.pause = true;

		this.totalOperations = 0;

	}

	public Repository getRepository() {
		return this.repository;
	}

	private void populateWordArray() {
		wordArray = new String[dictSize];

		try {
			final BufferedReader br = new BufferedReader(new FileReader(this.dictFile));
			String line;
			int i = 0;

			while ((line = br.readLine()) != null && i < dictSize) {
				wordArray[i] = line;
				i++;
			}

			if (i < dictSize) {
				dictSize = i;
			}

			br.close();

		} catch (final FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (final IOException e) {
			e.printStackTrace();
			System.exit(0);
		}

	}

	private synchronized void updateOperations(final int operations) {
		this.totalOperations += operations;
	}

	public synchronized int getTotalOperations() {
		return totalOperations;
	}

	public class Job implements Runnable {

		private final int put;
		private final int del;
		private final int get;
		private int count;
		private final Random rand;
		private volatile boolean paused;

		/**
		 * @param put percentage of insert article operations
		 * @param del percentage of remove article operations
		 * @param get percentage of find article operations, which is shared by
		 *            findByAuthor and findByKeyword
		 */
		public Job(final int put, final int del, final int get) {
			this.put = put;
			this.del = del;
			this.get = get;
			this.count = 0;
			try {
				Thread.sleep(100);
			} catch (final InterruptedException e) {
			}
			this.rand = new Random(System.nanoTime());
			paused = true;
		}

		private boolean contains(final List<String> list, final String word) {
			final Iterator<String> it = list.iterator();
			while (it.hasNext()) {
				if (it.next().compareTo(word) == 0)
					return true;
			}
			return false;
		}

		private Article generateArticle() {
			final int i = rand.nextInt(dictSize);
			final Article a = new Article(i, wordArray[i]);

			int nauthors = authors;
			while (nauthors > 0) {
				final int p = rand.nextInt(dictSize);
				final String word = wordArray[p];
				if (!contains(a.getAuthors(), word)) {
					a.addAuthor(word);
					nauthors--;
				}
			}

			int nkeywords = keywords;
			while (nkeywords > 0) {
				final int p = rand.nextInt(dictSize);
				final String word = wordArray[p];
				if (!contains(a.getKeywords(), word)) {
					a.addKeyword(word);
					nkeywords--;
				}
			}

			return a;
		}

		private List<String> generateListOfWords() {
			final List<String> res = new LinkedList<String>();
			int nwords = findList;

			while (nwords > 0) {
				final int p = rand.nextInt(dictSize);
				final String word = wordArray[p];
				if (!contains(res, word)) {
					res.add(word);
					nwords--;
				}
			}

			return res;
		}

		public void run() {

			while (pause) {
				Thread.yield();
			}

			paused = true;

			while (running) {

				if (DO_VALIDATION) {
					boolean done = false;

					while (pause) {
						if (!done) {
							System.out.println("Thread " + Thread.currentThread().getId() + ": Stoped");
							paused = true;
							done = true;
						}
					}
					paused = false;
				}

				final int op = rand.nextInt(100);

				if (op < put) {
					final Article a = generateArticle();
					repository.insertArticle(a);
				} else if (op < put + del) {
					final int id = rand.nextInt(dictSize);
					repository.removeArticle(id);
				} else if (op < put + del + (get / 2)) {
					final List<String> list = generateListOfWords();
					repository.findArticleByAuthor(list);
				} else {
					final List<String> list = generateListOfWords();
					repository.findArticleByKeyword(list);
				}

				count++;

			}

			updateOperations(count);

		}

	}

	public void spawnThread(final int nthreads) {
		workers = new Thread[nthreads];
		jobs = new Job[nthreads];

		for (int i = 0; i < nthreads; i++) {
			jobs[i] = new Job(put, del, get);
			workers[i] = new Thread(jobs[i]);
		}

		for (int i = 0; i < nthreads; i++) {
			workers[i].start();
		}

	}

	public void joinThreads() {
		for (int i = 0; i < workers.length; i++) {
			try {
				workers[i].join();
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void startTest() {
		this.running = true;
		this.pause = false;
	}

	public void stopTest() {
		this.running = false;
		this.pause = false;
	}

	public void pauseTest() {
		this.pause = true;

		while (true) {
			boolean ok = true;

			for (int i = 0; i < jobs.length; i++)
				if (!jobs[i].paused) {
					ok = false;
					break;
				}

			if (ok)
				break;
		}

	}

	public void restartTest() {
		this.pause = false;
	}
}
