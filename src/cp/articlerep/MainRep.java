package cp.articlerep;

/**
 * @author Ricardo Dias (2013)
 * @author João Lourenço (2020)
 */
public class MainRep {

	public static final boolean DO_VALIDATION = Boolean.parseBoolean(System
			.getProperty("cp.articlerep.validate"));

	
	public static void main(final String[] args) {

		if (args.length < 9) {
			System.out.println("usage: java" + MainRep.class.getCanonicalName()
					+ " time(sec) nthreads nkeys put(%) del(%) get(%) nauthors nkeywords nfindlist");
			System.exit(1);
		}

		final int time = Integer.parseInt(args[0]);
		final int nthreads = Integer.parseInt(args[1]);
		final int nkeys = Integer.parseInt(args[2]);
		final int put = Integer.parseInt(args[3]);
		final int del = Integer.parseInt(args[4]);
		final int get = Integer.parseInt(args[5]);

		if (put + del + get != 100) {
			System.out.println("Error: " + " put(%) + del(%) + get(%) must add to 100%");
			System.exit(1);
		}
		final int nauthors = Integer.parseInt(args[6]);
		final int nkeywords = Integer.parseInt(args[7]);
		final int nfindlist = Integer.parseInt(args[8]);

		final Worker run = new Worker(nkeys, "resources/dictionary.txt", put, del, get, nauthors, nkeywords, nfindlist);

		run.spawnThread(nthreads);

		run.startTest();

		final long start_time = System.currentTimeMillis();

		try {
			if (!DO_VALIDATION) {
				Thread.sleep(time * 1000);
			} else {
				for (int i = 0; i < time; i++) {
					Thread.sleep(1000);

					if ((i + 1) % 5 == 0) {
						run.pauseTest();

						System.out.println("[" + (i + 1) + "] -----------VALIDATION-----------");

						if (!run.getRepository().validate()) {
							System.out.println("[VALIDATION ERROR]");
							run.stopTest();
							return;

						}

						run.restartTest();

						System.out.println("Check done");
					}

				}
			}
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}

		run.stopTest();

		final long end_time = System.currentTimeMillis();

		final long total_time = (end_time - start_time) / 1000;
        
		System.out.println("[" + total_time +"] -----------VALIDATION-----------");

		if (!run.getRepository().validate()) {
			System.out.println("[VALIDATION ERROR]");
			run.stopTest();
			return;
        }

		run.joinThreads();

		System.out.println("Total time: " + total_time + " seconds");
		System.out.println("Operation rate: "
				+ Math.round(run.getTotalOperations()
						/ (double)total_time) + " ops/s");
	}

}
