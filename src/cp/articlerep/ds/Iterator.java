package cp.articlerep.ds;

/**
 * @author Ricardo Dias (2013)
 * @author João Lourenço (2020)
 */
public interface Iterator<V> {
	
	public boolean hasNext();
	public V next();

}
