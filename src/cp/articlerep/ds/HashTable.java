package cp.articlerep.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Ricardo Dias (2013)
 * @author João Lourenço (2020)
 */
public class HashTable<K extends Comparable<K>, V> implements Map<K, V> {

	private static class Node {
		public Object key;
		public Object value;
		public Node next;

		public Node(final Object key, final Object value, final Node next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}

	private Node[] table;
	private ReentrantLock[] lock;
	private String name;
	private int tableLen;
	
	public HashTable() {
		this(1000);
	}

	public HashTable(final int size) {
		this(size, "Anonymous");
	}

	public HashTable(final int size, final String name) {
		this.name = name;
		this.tableLen = size;
		this.table = new Node[size];
		this.lock = new ReentrantLock[size];
		for (int i = 0; i < size; i++)
			this.lock[i] = new ReentrantLock();
	}

	private int calcTablePos(final K key) {
		return Math.abs(key.hashCode()) % this.tableLen;
	}

	private void lockPos(final int pos) {
		lock[pos].lock();
	}

	private void unlockPos(final int pos) {
		lock[pos].unlock();
	}

	public void lockKey(final K key) {
		final int pos = this.calcTablePos(key);
		lockPos(pos);
	}

	public void unlockKey(final K key) {
		final int pos = this.calcTablePos(key);
		unlockPos(pos);
	}

	public void lockKey(final List<K> keyList) {
		final ArrayList<Integer> lpos = new ArrayList<Integer>();
		final Iterator<K> it = keyList.iterator();
		synchronized (this) {
			while (it.hasNext()) {
				final K key = it.next();
				final int p = calcTablePos(key);
				lpos.add(p);
			}
		}
		Collections.sort(lpos);
		for (final int pos : lpos) {
			lockPos(pos);
		}
	}

	public void unlockKey(final List<K> keyList) {
		final Iterator<K> it = keyList.iterator();
		synchronized (this) {
			while (it.hasNext()) {
				final K key = it.next();
				final int pos = calcTablePos(key);
				unlockPos(pos);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public V put(final K key, final V value) {
		final int pos = this.calcTablePos(key);
		Node n = this.table[pos];

		while (n != null && !n.key.equals(key)) {
			n = n.next;
		}

		if (n != null) {
			final V oldValue = (V) n.value;
			n.value = value;
			return oldValue;
		}

		final Node nn = new Node(key, value, this.table[pos]);
		this.table[pos] = nn;

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(final K key) {
		final int pos = this.calcTablePos(key);
		Node p = this.table[pos];
		if (p == null) {
			return null;
		}

		if (p.key.equals(key)) {
			this.table[pos] = p.next;
			return (V) p.value;
		}

		Node n = p.next;
		while (n != null && !n.key.equals(key)) {
			p = n;
			n = n.next;
		}

		if (n == null) {
			return null;
		}

		p.next = n.next;

		return (V) n.value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(final K key) {
		final int pos = this.calcTablePos(key);
		Node n = this.table[pos];
		while (n != null && !n.key.equals(key)) {
			n = n.next;
		}
		return (V) (n != null ? n.value : null);
	}

	@Override
	public boolean contains(final K key) {
		return get(key) != null;
	}

	/**
	 * No need to protect this method from concurrent interactions
	 */
	@Override
	public Iterator<V> values() {
		return new Iterator<V>() {

			private int pos = -1;
			private Node nextBucket = advanceToNextBucket();

			private Node advanceToNextBucket() {
				pos++;
				while (pos < HashTable.this.table.length && HashTable.this.table[pos] == null) {
					pos++;
				}
				if (pos < HashTable.this.table.length)
					return HashTable.this.table[pos];

				return null;
			}

			@Override
			public boolean hasNext() {
				return nextBucket != null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public V next() {
				final V result = (V) nextBucket.value;

				nextBucket = nextBucket.next != null ? nextBucket.next : advanceToNextBucket();

				return result;
			}

		};
	}

	@Override
	public Iterator<K> keys() {
		return null;
	}

}
