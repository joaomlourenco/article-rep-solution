package cp.articlerep.ds;

/**
 * @author Ricardo Dias (2013)
 * @author João Lourenço (2020)
 */
public interface Map<K extends Comparable<K>, V> {
	public V put(K key, V value);
	public boolean contains(K key);
	public V remove(K key);
	public V get(K key);
	public void lockKey(final K key);
	public void lockKey(final List<K> keyList);
	public void unlockKey(final K key);
	public void unlockKey(final List<K> keyList);

	public Iterator<V> values();
	public Iterator<K> keys();
}
