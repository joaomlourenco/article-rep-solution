package cp.articlerep;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import cp.articlerep.ds.Iterator;
import cp.articlerep.ds.LinkedList;
import cp.articlerep.ds.List;
import cp.articlerep.ds.Map;
import cp.articlerep.ds.HashTable;

/**
 * @author Ricardo Dias (2013)
 * @author João Lourenço (2020)
 */
public class Repository {

	private final Map<String, List<Article>> byAuthor;
	private final Map<String, List<Article>> byKeyword;
	private final Map<Integer, Article> byArticleId;

	public Repository(final int nkeys) {
		this.byAuthor = new HashTable<String, List<Article>>(40000, "byAuthor");
		this.byKeyword = new HashTable<String, List<Article>>(40000, "byKeyword");
		this.byArticleId = new HashTable<Integer, Article>(40000, "byArticleId");
	}

	public boolean insertArticle(final Article a) {

		// Lock the collision list for this article before checking if it exists
		byArticleId.lockKey(a.getId());
		if (byArticleId.contains(a.getId())) {
			// The article already exists in the repository
			byArticleId.unlockKey(a.getId());
			return false;
		}

		// Lock the collision lists for all the authors and keywords
		byAuthor.lockKey(a.getAuthors());
		byKeyword.lockKey(a.getKeywords());

		try {

			// Insert the article
			byArticleId.put(a.getId(), a);

			// Add the article to all the authors
			final Iterator<String> authors = a.getAuthors().iterator();
			while (authors.hasNext()) {
				final String name = authors.next();

				List<Article> ll = byAuthor.get(name);
				if (ll == null) {
					ll = new LinkedList<Article>();
					byAuthor.put(name, ll);
				}
				ll.add(a);
			}

			// Add the article to all the keywords
			final Iterator<String> keywords = a.getKeywords().iterator();
			while (keywords.hasNext()) {
				final String keyword = keywords.next();

				List<Article> ll = byKeyword.get(keyword);
				if (ll == null) {
					ll = new LinkedList<Article>();
					byKeyword.put(keyword, ll);
				}
				ll.add(a);
			}

			return true;
		} finally {
			// Unlock all the collision lists
			byKeyword.unlockKey(a.getKeywords());
			byAuthor.unlockKey(a.getAuthors());
			byArticleId.unlockKey(a.getId());
		}
	}

	public void removeArticle(final int id) {

		// Lock the collision list for this article before checking if it exists
		byArticleId.lockKey(id);
		final Article a = byArticleId.get(id);

		if (a == null) {
			// The article does not exists in the repository
			byArticleId.unlockKey(id);
			return;
		}

		// Lock the collision lists for all the authors and keywords
		byAuthor.lockKey(a.getAuthors());
		byKeyword.lockKey(a.getKeywords());

		try {

			// Remove the article
			byArticleId.remove(a.getId());

			// Remove the article from all the authors
			final Iterator<String> authors = a.getAuthors().iterator();
			while (authors.hasNext()) {
				final String name = authors.next();

				final List<Article> ll = byAuthor.get(name);
				if (ll != null) {
					int pos = 0;
					Iterator<Article> it = ll.iterator();
					while (it.hasNext()) {
						final Article toRem = it.next();
						if (toRem == a) {
							// System.out.println("Removing author" + toRem + "from " + pos);
							break;
						}
						pos++;
					}
					ll.remove(pos);
					it = ll.iterator();
					if (!it.hasNext()) { // checks if the list is empty
						byAuthor.remove(name);
					}
				} else {
					// System.out.println("Author list was empty for " + a);
				}
			}

			// Remove the article from all the keywords
			final Iterator<String> keywords = a.getKeywords().iterator();
			while (keywords.hasNext()) {
				final String keyword = keywords.next();

				final List<Article> ll = byKeyword.get(keyword);
				if (ll != null) {
					int pos = 0;
					Iterator<Article> it = ll.iterator();
					while (it.hasNext()) {
						final Article toRem = it.next();
						if (toRem == a) {
							// System.out.println("Removing keyword " + toRem + "from " + pos);
							break;
						}
						pos++;
					}
					ll.remove(pos);
					it = ll.iterator();
					if (!it.hasNext()) { // checks if the list is empty
						byKeyword.remove(keyword);
					}
				} else {
					// System.out.println("Keyword list was empty for " + a);
				}
			}
		} finally {
			byKeyword.unlockKey(a.getKeywords());
			byAuthor.unlockKey(a.getAuthors());
			byArticleId.unlockKey(a.getId());
		}
	}

	public List<Article> findArticleByAuthor(final List<String> authors) {

		byAuthor.lockKey(authors);

		final List<Article> res = new LinkedList<Article>();

		final Iterator<String> it = authors.iterator();
		while (it.hasNext()) {
			final String name = it.next();
			final List<Article> as = byAuthor.get(name);
			if (as != null) {
				final Iterator<Article> ait = as.iterator();
				while (ait.hasNext()) {
					final Article a = ait.next();
					res.add(a);
				}
			}
		}

		byAuthor.unlockKey(authors);

		return res;
	}

	public List<Article> findArticleByKeyword(final List<String> keywords) {

		byKeyword.lockKey(keywords);

		final List<Article> res = new LinkedList<Article>();

		final Iterator<String> it = keywords.iterator();
		while (it.hasNext()) {
			final String keyword = it.next();
			final List<Article> as = byKeyword.get(keyword);
			if (as != null) {
				final Iterator<Article> ait = as.iterator();
				while (ait.hasNext()) {
					final Article a = ait.next();
					res.add(a);
				}
			}
		}

		byKeyword.unlockKey(keywords);

		return res;
	}

	/**
	 * This method is supposed to be executed with no concurrent thread accessing
	 * the repository.
	 * 
	 */
	public boolean validate() {

		final HashSet<Integer> articleIds = new HashSet<Integer>();
		int articleCount = 0;

		final Iterator<Article> aIt = byArticleId.values();
		while (aIt.hasNext()) {
			final Article a = aIt.next();

			articleIds.add(a.getId());
			articleCount++;

			// check the authors consistency
			final Iterator<String> authIt = a.getAuthors().iterator();
			while (authIt.hasNext()) {
				final String name = authIt.next();
				if (!searchAuthorArticle(a, name)) {
					System.out.println("Article " + a + " from byArticleleId not present in byAuthor");
					return false;
				}
			}

			// check the keywords consistency
			final Iterator<String> keyIt = a.getKeywords().iterator();
			while (keyIt.hasNext()) {
				final String keyword = keyIt.next();
				if (!searchKeywordArticle(a, keyword)) {
					System.out.println("Article " + a + " from byArticleleId not present in byKeyword");
					return false;
				}
			}
		}

		// check if all the papers in byAuthor are in byArticleId
		final Iterator<List<Article>> auIt = byAuthor.values();
		while (auIt.hasNext()) {
			final List<Article> la = auIt.next();
			final Iterator<Article> it = la.iterator();
			while (it.hasNext()) {
				final Article a = it.next();
				if (byArticleId.get(a.getId()) == null) {
					System.out.println("Article " + a + " from byAuthor not present in byArticleId");
					return false;
				}
			}
		}

		// check if all the papers in byKeyword are in byArticleId
		final Iterator<List<Article>> kwIt = byKeyword.values();
		while (kwIt.hasNext()) {
			final List<Article> lkw = kwIt.next();
			final Iterator<Article> it = lkw.iterator();
			while (it.hasNext()) {
				final Article a = it.next();
				if (byArticleId.get(a.getId()) == null) {
					System.out.println("Article " + a + " from byKeyword not present in byArticleId");
					return false;
				}
			}
		}

		return articleCount == articleIds.size();
	}

	private boolean searchAuthorArticle(final Article a, final String author) {
		final List<Article> ll = byAuthor.get(author);
		if (ll != null) {
			final Iterator<Article> it = ll.iterator();
			while (it.hasNext()) {
				if (it.next() == a) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean searchKeywordArticle(final Article a, final String keyword) {
		final List<Article> ll = byKeyword.get(keyword);
		if (ll != null) {
			final Iterator<Article> it = ll.iterator();
			while (it.hasNext()) {
				if (it.next() == a) {
					return true;
				}
			}
		}
		return false;
	}

}
