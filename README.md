# README #

A concurrent Article Repository for the labs of Concurrency and Parallelism at DI-FCT-NOVA (www.di.fct.unl.pt), implementing a database over 3 concurrent hashmaps: 'in byArticleId', 'byAuthor' and 'byKeyword'.

To compile run `make`.

To execute run
    `java -Dcp.articlerep.validate=true -cp bin cp/articlerep/MainRep <time(sec)> <nthreads> <nkeys> <put(%)> <del(%)> <get(%)> <nauthors> <nkeywords> <nfindlist>`

Some examples are:

* A low contention write-only workload running 4 threads for 12 seconds:
        `java -Dcp.articlerep.validate=true -cp bin cp/articlerep/MainRep 12 4 20000 50 50 0 1 1 0`
* A high contention mixed workload running 2 threads for 5 seconds:
        `java -Dcp.articlerep.validate=true -cp bin cp/articlerep/MainRep 5 2 100 33 33 34 3 3 5`
* A high contention read-dominant workload running 8 threads for 20 seconds:
        `java -Dcp.articlerep.validate=true -cp bin cp/articlerep/MainRep 20 8 100 1 1 98 5 5 10`

The repository has multiple branches with a variety of working and non-working solutions (all versions will pass validation when running just on e thread):

* **master** — The initial version. The validation in this version is flaky, as it sometimes succeeds and sometime fails, and when it succeeds it does not mean the results are ok... it just means that although there may be problems the test did not find any problems.
* **validation** — An extension of 'master' branch with a stronger validation. Besides checking if all the articles in '*byArticleId*' are also present in '*byAuthor*' and '*byKeyword*', it also tests the other way around.  In this version the tests are not flaky anymore.  When there were concurrency problems it fails the validation.  When there were no concurrency problems the validation succeeds.
* **global_synchronized** — An extension of the 'validation' branch controlling concurrency by using a global lock (achieved by adding `synchronized` to the methods in the *Repository* class).  This version will not scale but it shall always pass the validation with any number of threads.
* **global_exclusive_reentrantlock** — An extension of the 'validation' branch controlling concurrency by using a global lock (achieved by creating a `reentrantLock` and adding a pair `lock`/`unlock` to the beginning and end of each method in the *Repository* class).  This version will not scale but it shall always pass the validation with any number of threads.
* **global_exclusive_reentrant_rw_lock** — — An extension of the 'global_exclusive_reentrantlock' branch controlling concurrency by using a global read-write lock (achieved by creating a `reentrantLock` and adding a pair `read/write`-`lock/unlock` to the beginning and end of each method in the *Repository* class).  This version will scale if the workload is read-dominant (e.g., 98% of read operations and `nfindlist=20`)
 * **per_hm_synchronized** — An extension of the 'validation' branch controlling concurrency by using 3 locks, one lock per hash map (achieved by adding `synchronized` to the methods in the *HashMap* class).  This version shall fail the validation when running with more than one thread.
* **per_list_synchronized** — An extension of the 'validation' branch controlling concurrency by using one lock per collision list in each of the threee hash maps (achieved by adding `synchronized` to the methods in the *LinkedList* class).  This version shall fail the validation when running with more than one thread.
* **per_list_all_locks_no_sort** — An extension of the 'validation' branch controlling concurrency by using one lock per collision list in each of the threee hash maps and acquiring all the locks upfront (achieved by creating a `reentrantLock` and adding a pair `lock`/`unlock` to the beginning and end of each method in the *LinkedList* class).  This version will most probably enter a deadlock state when running with more than one thread, noticeable by ptinting "VALIDATION" and never terminating.
* **per_list_all_locks_sort** — An extension of the 'validation' branch controlling concurrency by using one lock per collision list in each of the threee hash maps and acquiring all the locks upfront and in order (achieved by creating a `reentrantLock` and adding a pair `lock`/`unlock` to the beginning and end of each method in the *LinkedList* class).  **This version will scale well and it shall always pass the validation with any number of threads.**